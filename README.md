# Test framework created for the webinar
This project contains automated tests created to be shown at the webinar

The framework is written in Java based on the Spring Boot Framework, which implements dependency
injection mechanisms, obtaining the necessary properties from yml files, working with databases and much more (for 
more information, see the official developer's website: https://spring.io/projects/spring-boot)

#### Used libraries
* To send and receive responses from the API, TestRestTemplate is used, implemented in Spring Boot
* Spring Data JPA is used to work with the database
* JavaFaker is used to generate test data
* Allure Framework is used to generate reports on completed tests
* The postgresql driver is used to connect to the database
* Lombok is used for automatic generation of geters, seters and constructors
* AssertJ is used to verify the received data in the tests
* JUnit5 is used to implement and run tests
* Spring Profiles is used to select the startup configuration

#### Project structure
The framework has two root sources:
* main.java.ivan.smolin.webinar
* test.java.ivan.smolin.webinar

"main.java.ivan.smolin.webinar" root has the following structure:
* Package "**api**" (contains classes required for integration API tests)
* Package "**ui**" (contains classes necessary for integration UI tests)
* Class "**AppConfig**" (contains bin configurations needed to implement dependency injection)
* Resource root "**resources**" (contains yml configuration files with the data necessary to connect to the database
  and send API methods to the necessary endpoints, broken down by possible servers, as well as yml files necessary for
  localization testing of the application)

The "**api**" package contains packages divided by services:
* Package "**account**" (contains classes required for API testing of Account service)
* Package "**common**" (contains common classes used in testing all services)
* Package "**currency**" (contains classes required for the Currency service testing API)
* Package "**employee**" (contains classes required for the Employee service testing API)
* Package "**user**" (contains classes required for API testing of the User service)

The packages described above have the following structure:
* Package "**configuration**" (contains configuration classes for connecting to the necessary databases)
* Package "**constants**" (contains constant classes required for testing. Contains field names,
  validation messages, acceptable lengths for fields, and so on)
* Package "**entity**" (contains DAO entity classes for working with the database)
* Package "**model**" (contains classes of domain models for working with the API)
* Package "**random**" (contains helper classes for generating random data for each object)
* Package "**repository**" (contains interfaces for working with databases via Spring Data JPA)
* Package "**service**" (contains classes implementing methods for working with the API)

"test.java.ivan.smolin.webinar" root has the following structure:
* Package "**apiTests**" (tests for API validation are stored, test classes are divided into packages that test 
API methods of each business entity of the system)
* Package "**uiTests**" (tests are stored to test the UI, test classes are divided into packages that test the 
API methods of each business entity of the system)
* Class "**DbInitialization**" (tests are stored that are necessary only to generate test data for the
  webinar)

The file "**.gitignore**" stores the names of packages that will not be added to commits when pushing changes to the
GitLab repository

The "**.gitlab-ci.yml**" file stores the CI pipeline settings with broken jobs and scripts for them

In the file "**build.gradle**" describes plugins and libraries that are used in the project

The file "**lombok.config**" describes the configuration for Lombok

#### Local project setup
To configure the project locally, you need to follow the path:

`"Run/Debug Configuration" -> "Edit configuration templates..." -> "Gradle" -> "Environment variables"`

Add Value:
`LOCALE=en;PROFILE=test;DATABASE_USERNAME=postgres;DATABASE_PASSWORD=postgres`

The services that need to be launched for the correct testing are implemented in the following projects:
* https://gitlab.com/IvanSmolin/webinar-test-service-currency
* https://gitlab.com/IvanSmolin/webinar-test-service-employee
* https://gitlab.com/IvanSmolin/webinar-test-service-account

#### Building a project
Gradle is used to build the project.

The project implements the use of Gradle Wrapper, so when cloning the repository, there is no need to have
Gradle installed on the local machine.

All commands in the terminal must be run via `./gradlew`

#### Running tests via the terminal
To run the API tests, use the command in the terminal:
* `./gradlew clean test --tests "ivan.smolin.webinar.apitests.*"`

#### Viewing the test execution report via the terminal
To generate an Allure report, the Allure plugin in Gradle is used:
* `./gradlew allureServe`

#### Running tests in the IDE
You can also run certain tests or test classes by selecting them in the project structure or in the editor window
and right-click the command "Run..."
The results of the tests will be displayed in the "Run" tab