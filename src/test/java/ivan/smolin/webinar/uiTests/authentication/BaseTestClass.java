package ivan.smolin.webinar.uiTests.authentication;

import com.codeborne.selenide.Selenide;
import ivan.smolin.webinar.api.user.random.UserRandom;
import ivan.smolin.webinar.ui.authentication.pageObject.LoginPage;
import ivan.smolin.webinar.ui.authentication.resources.LoginPageResources;
import ivan.smolin.webinar.uiTests.BaseUiTestClass;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public abstract class BaseTestClass extends BaseUiTestClass {

    @Value("${uiUrl.authentication}")
    private String pageUrl;

    @Autowired
    protected LoginPage loginPage;

    @Autowired
    protected UserRandom userRandom;

    @Autowired
    protected LoginPageResources loginPageResources;

    @BeforeEach
    protected void setUp() {
        Selenide.open(pageUrl);
    }
}