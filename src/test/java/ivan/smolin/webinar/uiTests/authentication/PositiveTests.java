package ivan.smolin.webinar.uiTests.authentication;

import ivan.smolin.webinar.api.user.entity.UserEntity;
import ivan.smolin.webinar.ui.common.component.Label;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

@DisplayName("UI. Login. Позитивные тесты заполнения опросника NPS")
public final class PositiveTests extends BaseTestClass {
    private UserEntity entity;

    @AfterEach
    private void deleteTestValueFromDb() {
//        userRepository.delete(entity);
    }

    @Test
    @DisplayName("UI. Login. Пользователь успешно входит в систему")
    public void testLoginSuccess() {
        loginPage.register();
        new Label(loginPageResources.getWelcomeText()).shouldBeVisible();
    }

    @Test
    @DisplayName("UI. Login. Пользователь успешно создаёт аккаунт")
    public void testCreateAccountSuccess() {
        UserEntity randomEntity = userRandom.getRandomEntity();
        loginPage.createAccount(randomEntity)
                .fillPersonalInformation(randomEntity)
                .fillAddressModule(randomEntity)
                .send();
//        entity = userRepository.findByEmail(randomEntity.getEmail());
//        assertThat(randomEntity).isEqualTo(entity);
    }
}