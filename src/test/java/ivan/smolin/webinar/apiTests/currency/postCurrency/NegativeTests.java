package ivan.smolin.webinar.apiTests.currency.postCurrency;

import ivan.smolin.webinar.apiTests.currency.CurrencyApiBaseTestClass;
import ivan.smolin.webinar.api.currency.model.CurrencyModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertResponseWithErrorMessages;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.fieldIsRequired;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.listOfObjectsIsRequired;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.maxLength;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.minLength;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.objectIsRequired;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.COUNTRY;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.COUNTRY_NAME;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.EXCHANGE_RATES;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.NAME;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.NAME_MAX_LENGTH;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.NAME_MIN_LENGTH;

@DisplayName("API. Currency. POST '/currency'. Негативные тесты создания валюты")
public final class NegativeTests extends CurrencyApiBaseTestClass {

    @Test
    @DisplayName("API. Currency. POST '/currency'. Все атрибуты тела запроса равны null")
    public void testAttributesAreNullFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY, objectIsRequired(COUNTRY));
            put(EXCHANGE_RATES, listOfObjectsIsRequired(EXCHANGE_RATES));
            put(NAME, fieldIsRequired(NAME));
        }};

        ResponseEntity<String> responseEntity = postCurrencyService.postError(new CurrencyModel());
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Currency. POST '/currency'. Атрибуты тела запроса имеют строки с превышением максимальной длины")
    public void testAttributesExceededMaxLengthFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, maxLength(NAME_MAX_LENGTH));
            put(NAME, maxLength(NAME_MAX_LENGTH));
        }};

        CurrencyModel requestModel = currencyRandom.getRandomModelExceeded();
        ResponseEntity<String> responseEntity = postCurrencyService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Currency. POST '/currency'. Атрибуты тела запроса имеют длину строк меньше минимальной длины")
    public void testAttributesLessMinLengthFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, minLength(NAME_MIN_LENGTH));
            put(NAME, minLength(CURRENCY_NAME_MIN_LENGTH));
        }};

        CurrencyModel requestModel = currencyRandom.getRandomModelLessMin();
        ResponseEntity<String> responseEntity = postCurrencyService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }
}