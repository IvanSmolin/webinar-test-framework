package ivan.smolin.webinar.apiTests.currency.deleteCurrency;

import ivan.smolin.webinar.apiTests.currency.CurrencyApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertBadRequestStringResponse;
import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertNotFoundStringResponse;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Currency. DELETE '/currency/{id}'. Негативные тесты удаления валюты по ID")
public final class NegativeTests extends CurrencyApiBaseTestClass {

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. Неизвестный ID")
    public void testUnknownIdFailed() {
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(INTEGER_MAX_LENGTH);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID больше граничного значения для int типа")
    public void testNotIntegerIdFailed() {
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID равен нулю")
    public void testZeroIdFailed() {
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(ZERO_NUMBER);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID - отрицательное число")
    public void testNegativeIdFailed() {
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(currencyRandom.getNegativeNumber());
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID - не числовое значение с минимальной длиной")
    public void testStringMinIdFailed() {
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(currencyRandom.getTextString(MIN_NUMBER));
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. ID - не числовое значение с максимальной длиной")
    public void testStringMaxIdFailed() {
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(currencyRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestStringResponse(responseEntity);
    }
}