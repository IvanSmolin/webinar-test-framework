package ivan.smolin.webinar.apiTests.currency.getCurrency;

import ivan.smolin.webinar.apiTests.currency.CurrencyApiBaseTestClass;
import ivan.smolin.webinar.api.currency.model.CurrencyModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertOkResponse;

@DisplayName("API. Currency. GET '/currency/{id}'. Позитивные тесты получения валюты по ID")
public final class PositiveTests extends CurrencyApiBaseTestClass {

    @Test
    @DisplayName("API. Currency. GET '/currency/{id}'. Получение валюты по ID")
    public void testSuccess() {
        CurrencyModel model = currencyRepository.getRandomActiveCurrency().toCurrencyModel();
        ResponseEntity<CurrencyModel> responseEntity = getCurrencyService.get(model.getId());
        assertOkResponse(responseEntity, model);
    }
}