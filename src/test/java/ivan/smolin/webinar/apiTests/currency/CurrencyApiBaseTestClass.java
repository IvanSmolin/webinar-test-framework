package ivan.smolin.webinar.apiTests.currency;

import ivan.smolin.webinar.api.currency.random.CurrencyRandom;
import ivan.smolin.webinar.api.currency.repository.CurrencyRepository;
import ivan.smolin.webinar.api.currency.service.DeleteCurrencyService;
import ivan.smolin.webinar.api.currency.service.GetCurrencyService;
import ivan.smolin.webinar.api.currency.service.PostCurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class CurrencyApiBaseTestClass {

    /**
     * Repositories
     */
    @Autowired
    protected CurrencyRepository currencyRepository;

    /**
     * Random Helpers
     */
    @Autowired
    protected CurrencyRandom currencyRandom;

    /**
     * Services
     */
    @Autowired
    protected PostCurrencyService postCurrencyService;

    @Autowired
    protected GetCurrencyService getCurrencyService;

    @Autowired
    protected DeleteCurrencyService deleteCurrencyService;
}