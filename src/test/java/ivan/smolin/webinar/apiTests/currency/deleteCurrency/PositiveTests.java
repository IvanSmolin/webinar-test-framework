package ivan.smolin.webinar.apiTests.currency.deleteCurrency;

import ivan.smolin.webinar.apiTests.currency.CurrencyApiBaseTestClass;
import ivan.smolin.webinar.api.currency.entity.CurrencyEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("API. Currency. DELETE '/currency/{id}'. Позитивные тесты удаления валюты по ID")
public final class PositiveTests extends CurrencyApiBaseTestClass {

    @Test
    @DisplayName("API. Currency. DELETE '/currency/{id}'. Удаление валюты по ID")
    public void testSuccess() {
        CurrencyEntity entity = currencyRandom.getRandomEntity();
        currencyRepository.save(entity);
        ResponseEntity<String> responseEntity = deleteCurrencyService.delete(entity.getId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}