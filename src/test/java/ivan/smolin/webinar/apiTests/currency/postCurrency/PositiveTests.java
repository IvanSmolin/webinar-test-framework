package ivan.smolin.webinar.apiTests.currency.postCurrency;

import ivan.smolin.webinar.apiTests.currency.CurrencyApiBaseTestClass;
import ivan.smolin.webinar.api.currency.entity.CurrencyEntity;
import ivan.smolin.webinar.api.currency.model.CurrencyModel;
import ivan.smolin.webinar.api.currency.model.CurrencyResponseModel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertCreatedResponse;

@DisplayName("API. Currency. POST '/currency'. Позитивные тесты создания валюты")
public final class PositiveTests extends CurrencyApiBaseTestClass {
    private CurrencyEntity entity;

    @AfterEach
    private void deleteTestValueFromDb() {
        currencyRepository.delete(entity);
    }

    @Test
    @DisplayName("API. Currency. POST '/currency'. Создание валюты с максимальными данными")
    public void testMaxBodySuccess() {
        CurrencyModel requestModel = currencyRandom.getRandomModelMax();
        ResponseEntity<CurrencyResponseModel> responseEntity = postCurrencyService.post(requestModel);
        entity = currencyRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Currency. POST '/currency'. Создание валюты с минимальными данными")
    public void testMinBodySuccess() {
        CurrencyModel requestModel = currencyRandom.getRandomModelMin();
        ResponseEntity<CurrencyResponseModel> responseEntity = postCurrencyService.post(requestModel);
        entity = currencyRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }
}