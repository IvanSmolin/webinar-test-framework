package ivan.smolin.webinar.apiTests.account;

import ivan.smolin.webinar.api.account.random.AccountRandom;
import ivan.smolin.webinar.api.account.repository.AccountRepository;
import ivan.smolin.webinar.api.account.service.DeleteAccountService;
import ivan.smolin.webinar.api.account.service.GetAccountService;
import ivan.smolin.webinar.api.account.service.PostAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class AccountApiBaseTestClass {

    /**
     * Repositories
     */
    @Autowired
    protected AccountRepository accountRepository;

    /**
     * Random Helpers
     */
    @Autowired
    protected AccountRandom accountRandom;

    /**
     * Services
     */
    @Autowired
    protected PostAccountService postAccountService;

    @Autowired
    protected GetAccountService getAccountService;

    @Autowired
    protected DeleteAccountService deleteAccountService;
}