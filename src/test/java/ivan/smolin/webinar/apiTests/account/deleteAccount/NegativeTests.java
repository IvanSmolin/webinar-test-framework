package ivan.smolin.webinar.apiTests.account.deleteAccount;

import ivan.smolin.webinar.apiTests.account.AccountApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertBadRequestStringResponse;
import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertNotFoundStringResponse;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Account. DELETE '/account/{id}'. Негативные тесты удаления аккаунта по ID")
public final class NegativeTests extends AccountApiBaseTestClass {

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. Неизвестный ID")
    public void testUnknownIdFailed() {
        ResponseEntity<String> responseEntity = deleteAccountService.delete(INTEGER_MAX_LENGTH);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID больше граничного значения для int типа")
    public void testNotIntegerIdFailed() {
        ResponseEntity<String> responseEntity = deleteAccountService.delete(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID равен нулю")
    public void testZeroIdFailed() {
        ResponseEntity<String> responseEntity = deleteAccountService.delete(ZERO_NUMBER);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID - отрицательное число")
    public void testNegativeIdFailed() {
        ResponseEntity<String> responseEntity = deleteAccountService.delete(accountRandom.getNegativeNumber());
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID - не числовое значение с минимальной длиной")
    public void testStringMinIdFailed() {
        ResponseEntity<String> responseEntity = deleteAccountService.delete(accountRandom.getTextString(MIN_NUMBER));
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. ID - не числовое значение с максимальной длиной")
    public void testStringMaxIdFailed() {
        ResponseEntity<String> responseEntity = deleteAccountService.delete(accountRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestStringResponse(responseEntity);
    }
}