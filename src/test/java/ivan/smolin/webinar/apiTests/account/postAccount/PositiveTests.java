package ivan.smolin.webinar.apiTests.account.postAccount;

import ivan.smolin.webinar.apiTests.account.AccountApiBaseTestClass;
import ivan.smolin.webinar.api.account.entity.AccountEntity;
import ivan.smolin.webinar.api.account.model.AccountModel;
import ivan.smolin.webinar.api.account.model.AccountResponseModel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertCreatedResponse;

@DisplayName("API. Account. POST '/account'. Позитивные тесты создания аккаунта")
public final class PositiveTests extends AccountApiBaseTestClass {
    private AccountEntity entity;

    @AfterEach
    private void deleteTestValueFromDb() {
        accountRepository.delete(entity);
    }

    @Test
    @DisplayName("API. Account. POST '/account'. Создание аккаунта с максимальными данными")
    public void testMaxBodySuccess() {
        AccountModel requestModel = accountRandom.getRandomModelMax();
        ResponseEntity<AccountResponseModel> responseEntity = postAccountService.post(requestModel);
        entity = accountRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Account. POST '/account'. Создание аккаунта с минимальными данными")
    public void testMinBodySuccess() {
        AccountModel requestModel = accountRandom.getRandomModelMin();
        ResponseEntity<AccountResponseModel> responseEntity = postAccountService.post(requestModel);
        entity = accountRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }
}