package ivan.smolin.webinar.apiTests.account.deleteAccount;

import ivan.smolin.webinar.apiTests.account.AccountApiBaseTestClass;
import ivan.smolin.webinar.api.account.entity.AccountEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("API. Account. DELETE '/account/{id}'. Позитивные тесты удаления аккаунта по ID")
public final class PositiveTests extends AccountApiBaseTestClass {

    @Test
    @DisplayName("API. Account. DELETE '/account/{id}'. Удаление аккаунта по ID")
    public void testSuccess() {
        AccountEntity entity = accountRandom.getRandomEntity();
        accountRepository.save(entity);
        ResponseEntity<String> responseEntity = deleteAccountService.delete(entity.getId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}