package ivan.smolin.webinar.apiTests.account.getAccount;

import ivan.smolin.webinar.apiTests.account.AccountApiBaseTestClass;
import ivan.smolin.webinar.api.account.model.AccountModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertOkResponse;

@DisplayName("API. Account. GET '/account/{id}'. Позитивные тесты получения аккаунта по ID")
public final class PositiveTests extends AccountApiBaseTestClass {

    @Test
    @DisplayName("API. Account. GET '/account/{id}'. Получение аккаунта по ID")
    public void testSuccess() {
        AccountModel model = accountRepository.getRandomActiveAccount().toAccountModel();
        ResponseEntity<AccountModel> responseEntity = getAccountService.get(model.getId());
        assertOkResponse(responseEntity, model);
    }
}