package ivan.smolin.webinar.apiTests.employee;

import ivan.smolin.webinar.api.employee.random.EmployeeRandom;
import ivan.smolin.webinar.api.employee.repository.EmployeeRepository;
import ivan.smolin.webinar.api.employee.service.DeleteEmployeeService;
import ivan.smolin.webinar.api.employee.service.GetEmployeeService;
import ivan.smolin.webinar.api.employee.service.PostEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public abstract class EmployeeApiBaseTestClass {

    /**
     * Repositories
     */
    @Autowired
    protected EmployeeRepository employeeRepository;

    /**
     * Random Helpers
     */
    @Autowired
    protected EmployeeRandom employeeRandom;

    /**
     * Services
     */
    @Autowired
    protected PostEmployeeService postEmployeeService;

    @Autowired
    protected GetEmployeeService getEmployeeService;

    @Autowired
    protected DeleteEmployeeService deleteEmployeeService;
}