package ivan.smolin.webinar.apiTests.employee.deleteEmployee;

import ivan.smolin.webinar.apiTests.employee.EmployeeApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertBadRequestStringResponse;
import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertNotFoundStringResponse;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Employee. DELETE '/employee/{id}'. Негативные тесты удаления сотрудника по ID")
public final class NegativeTests extends EmployeeApiBaseTestClass {

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. Неизвестный ID")
    public void testUnknownIdFailed() {
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(INTEGER_MAX_LENGTH);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. ID больше граничного значения для int типа")
    public void testNotIntegerIdFailed() {
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. ID равен нулю")
    public void testZeroIdFailed() {
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(ZERO_NUMBER);
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. ID - отрицательное число")
    public void testNegativeIdFailed() {
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(employeeRandom.getNegativeNumber());
        assertNotFoundStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. ID - не числовое значение с минимальной длиной")
    public void testStringMinIdFailed() {
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(employeeRandom.getTextString(MIN_NUMBER));
        assertBadRequestStringResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. ID - не числовое значение с максимальной длиной")
    public void testStringMaxIdFailed() {
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(employeeRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestStringResponse(responseEntity);
    }
}