package ivan.smolin.webinar.apiTests.employee.deleteEmployee;

import ivan.smolin.webinar.apiTests.employee.EmployeeApiBaseTestClass;
import ivan.smolin.webinar.api.employee.entity.EmployeeEntity;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayName("API. Employee. DELETE '/employee/{id}'. Позитивные тесты удаления сотрудника по ID")
public final class PositiveTests extends EmployeeApiBaseTestClass {

    @Test
    @DisplayName("API. Employee. DELETE '/employee/{id}'. Удаление сотрудника по ID")
    public void testSuccess() {
        EmployeeEntity entity = employeeRandom.getRandomEntity();
        employeeRepository.save(entity);
        ResponseEntity<String> responseEntity = deleteEmployeeService.delete(entity.getId());
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }
}