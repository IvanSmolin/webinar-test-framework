package ivan.smolin.webinar.apiTests.employee.getEmployee;

import ivan.smolin.webinar.apiTests.employee.EmployeeApiBaseTestClass;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertBadRequestResponse;
import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertNotFoundResponse;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_EXCEEDED_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.INTEGER_MAX_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@DisplayName("API. Employee. GET '/employee/{id}'. Негативные тесты получения сотрудника по ID")
public final class NegativeTests extends EmployeeApiBaseTestClass {

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. Неизвестный ID")
    public void testUnknownIdFailed() {
        ResponseEntity<String> responseEntity = getEmployeeService.getError(INTEGER_MAX_LENGTH);
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. ID больше граничного значения для int типа")
    public void testNotIntegerIdFailed() {
        ResponseEntity<String> responseEntity = getEmployeeService.getError(INTEGER_EXCEEDED_LENGTH);
        assertBadRequestResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. ID равен нулю")
    public void testZeroIdFailed() {
        ResponseEntity<String> responseEntity = getEmployeeService.getError(ZERO_NUMBER);
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. ID - отрицательное число")
    public void testNegativeIdFailed() {
        ResponseEntity<String> responseEntity = getEmployeeService.getError(employeeRandom.getNegativeNumber());
        assertNotFoundResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. ID - не числовое значение с минимальной длиной")
    public void testStringMinIdFailed() {
        ResponseEntity<String> responseEntity = getEmployeeService.getError(employeeRandom.getTextString(MIN_NUMBER));
        assertBadRequestResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. GET '/employee/{id}'. ID - не числовое значение с максимальной длиной")
    public void testStringMaxIdFailed() {
        ResponseEntity<String> responseEntity = getEmployeeService.getError(employeeRandom.getTextString(MAX_URL_LENGTH));
        assertBadRequestResponse(responseEntity);
    }
}