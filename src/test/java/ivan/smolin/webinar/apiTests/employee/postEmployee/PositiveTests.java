package ivan.smolin.webinar.apiTests.employee.postEmployee;

import ivan.smolin.webinar.apiTests.employee.EmployeeApiBaseTestClass;
import ivan.smolin.webinar.api.employee.entity.EmployeeEntity;
import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import ivan.smolin.webinar.api.employee.model.EmployeeResponseModel;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertCreatedResponse;

@DisplayName("API. Employee. POST '/employee'. Позитивные тесты создания сотрудника")
public final class PositiveTests extends EmployeeApiBaseTestClass {
    private EmployeeEntity entity;

    @AfterEach
    private void deleteTestValueFromDb() {
        employeeRepository.delete(entity);
    }

    @Test
    @DisplayName("API. Employee. POST '/employee'. Создание сотрудника с максимальными данными")
    public void testMaxBodySuccess() {
        EmployeeModel requestModel = employeeRandom.getRandomModelMax();
        ResponseEntity<EmployeeResponseModel> responseEntity = postEmployeeService.post(requestModel);
        entity = employeeRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }

    @Test
    @DisplayName("API. Employee. POST '/employee'. Создание сотрудника с минимальными данными")
    public void testMinBodySuccess() {
        EmployeeModel requestModel = employeeRandom.getRandomModelMin();
        ResponseEntity<EmployeeResponseModel> responseEntity = postEmployeeService.post(requestModel);
        entity = employeeRepository.findById(responseEntity.getBody().getId()).get();
        assertCreatedResponse(responseEntity);
    }
}