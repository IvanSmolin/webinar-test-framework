package ivan.smolin.webinar.apiTests.employee.postEmployee;

import ivan.smolin.webinar.apiTests.employee.EmployeeApiBaseTestClass;
import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static ivan.smolin.webinar.api.common.CustomHttpResponseAssertion.assertResponseWithErrorMessages;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.fieldIsRequired;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.listOfObjectsIsRequired;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.maxLength;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.minLength;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.objectIsRequired;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.COUNTRY;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.COUNTRY_NAME;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.EXCHANGE_RATES;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.NAME;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.NAME_MAX_LENGTH;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.NAME_MIN_LENGTH;

@DisplayName("API. Employee. POST '/employee'. Негативные тесты создания сотрудника")
public final class NegativeTests extends EmployeeApiBaseTestClass {

    @Test
    @DisplayName("API. Employee. POST '/employee'. Все атрибуты тела запроса равны null")
    public void testAttributesAreNullFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY, objectIsRequired(COUNTRY));
            put(EXCHANGE_RATES, listOfObjectsIsRequired(EXCHANGE_RATES));
            put(NAME, fieldIsRequired(NAME));
        }};

        ResponseEntity<String> responseEntity = postEmployeeService.postError(new EmployeeModel());
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Employee. POST '/employee'. Атрибуты тела запроса имеют строки с превышением максимальной длины")
    public void testAttributesExceededMaxLengthFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, maxLength(NAME_MAX_LENGTH));
            put(NAME, maxLength(NAME_MAX_LENGTH));
        }};

        EmployeeModel requestModel = employeeRandom.getRandomModelExceeded();
        ResponseEntity<String> responseEntity = postEmployeeService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }

    @Test
    @DisplayName("API. Employee. POST '/employee'. Атрибуты тела запроса имеют длину строк меньше минимальной длины")
    public void testAttributesLessMinLengthFailed() {
        Map<String, String> errorMap = new HashMap<String, String>() {{
            put(COUNTRY_NAME, minLength(NAME_MIN_LENGTH));
            put(NAME, minLength(CURRENCY_NAME_MIN_LENGTH));
        }};

        EmployeeModel requestModel = employeeRandom.getRandomModelLessMin();
        ResponseEntity<String> responseEntity = postEmployeeService.postError(requestModel);
        assertResponseWithErrorMessages(responseEntity, errorMap);
    }
}