package ivan.smolin.webinar;

import ivan.smolin.webinar.api.account.random.AccountRandom;
import ivan.smolin.webinar.api.account.repository.AccountRepository;
import ivan.smolin.webinar.api.currency.random.CurrencyRandom;
import ivan.smolin.webinar.api.currency.repository.CurrencyRepository;
import ivan.smolin.webinar.api.employee.random.EmployeeRandom;
import ivan.smolin.webinar.api.employee.repository.EmployeeRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.*;

@SpringBootTest
public final class DbInitialization {

    @Autowired
    private CurrencyRandom currencyRandom;

    @Autowired
    private EmployeeRandom employeeRandom;

    @Autowired
    private AccountRandom accountRandom;

    @Autowired
    private CurrencyRepository currencyRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private AccountRepository accountRepository;

    @Test
    public void initializeCurrencyDatabase() {
        currencyRepository.saveAll(
                Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                        .limit(MAX_LIST_LENGTH)
                        .map(it -> currencyRandom.getRandomEntity())
                        .collect(Collectors.toList())
        );
    }

    @Test
    public void initializeEmployeeDatabase() {
        employeeRepository.saveAll(
                Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                        .limit(MAX_LIST_LENGTH)
                        .map(it -> employeeRandom.getRandomEntity())
                        .collect(Collectors.toList())
        );
    }

    @Test
    public void initializeAccountDatabase() {
        accountRepository.saveAll(
                Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                        .limit(MAX_LIST_LENGTH)
                        .map(it -> accountRandom.getRandomEntity())
                        .collect(Collectors.toList())
        );
    }
}