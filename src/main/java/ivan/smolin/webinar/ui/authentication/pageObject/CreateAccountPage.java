package ivan.smolin.webinar.ui.authentication.pageObject;

import ivan.smolin.webinar.api.user.entity.UserEntity;
import ivan.smolin.webinar.ui.authentication.resources.CreateAccountPageResources;
import ivan.smolin.webinar.ui.authentication.resources.LoginPageResources;
import ivan.smolin.webinar.ui.authentication.module.createAccount.AddressModule;
import ivan.smolin.webinar.ui.authentication.module.createAccount.PersonalInformationModule;
import ivan.smolin.webinar.ui.common.component.Button;
import lombok.Getter;
import org.springframework.stereotype.Component;

@Getter
@Component
public final class CreateAccountPage {
    private final PersonalInformationModule personalInformationModule;
    private final AddressModule addressModule;
    private final Button register;

    public CreateAccountPage(
            LoginPageResources loginPageResources,
            CreateAccountPageResources createAccountPageResources
    ) {
        personalInformationModule = new PersonalInformationModule(
                loginPageResources,
                createAccountPageResources
        );
        addressModule = new AddressModule(createAccountPageResources);
        register = new Button(createAccountPageResources.getRegister());
    }

    public CreateAccountPage fillPersonalInformation(UserEntity user) {
        personalInformationModule.getTitle().selectRadioWithText(user.getTitle().getName());
        personalInformationModule.getFirstName().sendKeys(user.getFirstName());
        personalInformationModule.getLastName().sendKeys(user.getLastName());
        personalInformationModule.getEmail().sendKeys(user.getEmail());
        personalInformationModule.getPassword().sendKeys(user.getPassword());

        return this;
    }

    public CreateAccountPage fillAddressModule(UserEntity user) {
        addressModule.getFirstName().sendKeys(user.getAddressFirstName());
        addressModule.getLastName().sendKeys(user.getAddressLastName());
        addressModule.getCompany().sendKeys(user.getCompany());
        addressModule.getAddress().sendKeys(user.getAddress());
        addressModule.getSecondAddress().sendKeys(user.getSecondAddress());
        addressModule.getCity().sendKeys(user.getCity());
        addressModule.getState().selectOptionByText(user.getState());
        addressModule.getZip().sendKeys(user.getZip());
        addressModule.getInfo().sendKeys(user.getAdditionalInformation());
        addressModule.getHomePhone().sendKeys(user.getHomePhone());
        addressModule.getMobilePhone().sendKeys(user.getMobilePhone());
        addressModule.getAlias().sendKeys(user.getAlias());

        return this;
    }

    public void send() {
        register.click();
    }
}