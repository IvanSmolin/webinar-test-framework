package ivan.smolin.webinar.ui.authentication.pageObject;

import ivan.smolin.webinar.api.user.entity.UserEntity;
import ivan.smolin.webinar.ui.authentication.resources.LoginPageResources;
import ivan.smolin.webinar.ui.authentication.module.login.CreateAccountModule;
import ivan.smolin.webinar.ui.authentication.module.login.LoginModule;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public final class LoginPage {
    private final CreateAccountModule createAccountModule;
    private final LoginModule loginModule;

    @Autowired
    private CreateAccountPage createAccountPage;

    public LoginPage(LoginPageResources loginPageResources) {
        this.createAccountModule = new CreateAccountModule(loginPageResources);
        this.loginModule = new LoginModule(loginPageResources);
    }

    public void register() {
        loginModule.getEmail().sendKeys("new@test.ru");
        loginModule.getPassword().sendKeys("password");
        loginModule.getSignIn().click();
    }

    public CreateAccountPage createAccount(UserEntity user) {
        createAccountModule.getEmail().sendKeys(user.getEmail());
        createAccountModule.getCreateAccount().click();

        return createAccountPage;
    }
}