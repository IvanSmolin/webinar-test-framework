package ivan.smolin.webinar.ui.common.component;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byTagName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public final class Button extends BaseComponent {

    public Button(String text) {
        this.name = text;
        this.element = $(byText(name)).parent().as(getComponentAlias());
    }

    public Button(String text, SelenideElement element) {
        this.name = text;
        this.element = element.$$(byTagName("button")).findBy(Condition.text(name)).as(getComponentAlias());
    }

    public void click() {
        this.element.click();
    }
}