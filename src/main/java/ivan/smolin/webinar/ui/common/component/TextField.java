package ivan.smolin.webinar.ui.common.component;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byTagName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public final class TextField extends BaseComponent {

    public TextField(String text) {
        this.name = text;
        this.element = getElement($(byText(name)));
    }

    public TextField(String text, SelenideElement element) {
        this.name = text;
        this.element = getElement(element.$(byText(name)));
    }

    public void sendKeys(String value) {
        getInput().setValue(value);
    }

    private SelenideElement getInput() {
        return element.$(byTagName("input")).as(getComponentAlias());
    }

    private SelenideElement getElement(SelenideElement element) {
        return element.parent().as(getComponentAlias());
    }
}