package ivan.smolin.webinar.ui.common.component;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byTagName;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

public final class Dropdown extends BaseComponent {

    public Dropdown(String text) {
        this.name = text;
        this.element = getElement($(byText(name)));
    }

    public Dropdown(String text, SelenideElement element) {
        this.name = text;
        this.element = getElement(element.$(byText(name)));
    }

    public void openOptions() {
        element.$(byTagName("select")).as(getComponentAlias()).click();
    }

    public void selectOptionByText(String text) {
        openOptions();
        element.$(byText(text)).as(getComponentAlias() + " value '" + text + "'").click();
    }

    public void selectRandomOption() {
        openOptions();
        ElementsCollection elements = getOptions();
        elements.get((int) (Math.random() * elements.size())).as(getComponentAlias() + " select random value").click();
    }

    private SelenideElement getElement(SelenideElement element) {
        return element.parent().as(getComponentAlias());
    }

    private ElementsCollection getOptions() {
        return element.$$(byTagName("option"));
    }
}