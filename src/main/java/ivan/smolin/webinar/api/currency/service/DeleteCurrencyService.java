package ivan.smolin.webinar.api.currency.service;

import ivan.smolin.webinar.api.common.BaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class DeleteCurrencyService extends BaseService {

    public ResponseEntity<String> delete(long id) {
        return delete(String.valueOf(id));
    }

    public ResponseEntity<String> delete(String id) {
        return deleteRequest(
                appProperties.getCurrencyBaseUrl() + "/" + id
        );
    }
}