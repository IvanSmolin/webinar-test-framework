package ivan.smolin.webinar.api.currency.entity;

import ivan.smolin.webinar.api.currency.model.CurrencyExchangeRateModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "currency_exchange_rate")
public final class CurrencyExchangeRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Double rate;

    @Column(name = "rate_date")
    private LocalDate rateDate;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public CurrencyExchangeRateModel toCurrencyExchangeRateModel() {
        return new CurrencyExchangeRateModel()
                .setId(this.id)
                .setRate(this.rate)
                .setRateDate(this.rateDate.toString())
                .setCreatedAt(this.createdAt.toString());
    }
}