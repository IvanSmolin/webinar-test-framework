package ivan.smolin.webinar.api.currency.repository;

import ivan.smolin.webinar.api.currency.entity.CurrencyEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CurrencyRepository extends CrudRepository<CurrencyEntity, Integer> {

    @Query(value = "select * " +
            "from currency " +
            "where active = true " +
            "order by random() " +
            "limit 1",
            nativeQuery = true)
    CurrencyEntity getRandomActiveCurrency();

    @Query("SELECT e " +
            "FROM CurrencyEntity e " +
            "WHERE e.active = true")
    List<CurrencyEntity> getAllActiveEntities();

    List<CurrencyEntity> findAllByActiveTrue();

    @Query("SELECT e " +
            "FROM CurrencyEntity e " +
            "WHERE e.name = ?1")
    CurrencyEntity getByName(String id);

    CurrencyEntity findByName(String name);
}