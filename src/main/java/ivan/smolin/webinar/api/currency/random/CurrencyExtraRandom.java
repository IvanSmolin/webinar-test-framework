package ivan.smolin.webinar.api.currency.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.currency.entity.CurrencyExtraEntity;
import ivan.smolin.webinar.api.currency.model.CurrencyExtraModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@Component
public final class CurrencyExtraRandom extends CommonRandom {

    /**
     * Метод для генерации Currency Extra Entity
     */
    private CurrencyExtraEntity getRandomEntity() {
        return new CurrencyExtraEntity()
                .setName(faker.name().name())
                .setCreatedAt(getDate());
    }

    public List<CurrencyExtraEntity> getRandomListEntity() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomEntity())
                .collect(Collectors.toList());
    }

    /**
     * Методы для генерации Currency Extra Model
     */
    private CurrencyExtraModel getRandomModel() {
        return new CurrencyExtraModel()
                .setName(faker.name().name())
                .setCreatedAt(getStringDate());
    }

    public List<CurrencyExtraModel> getRandomListModel() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomModel())
                .collect(Collectors.toList());
    }
}