package ivan.smolin.webinar.api.currency.configuration;

import com.zaxxer.hikari.HikariDataSource;
import ivan.smolin.webinar.api.common.BaseConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(
        basePackages = "ivan.smolin.webinar.api.currency.repository",
        entityManagerFactoryRef = "currencyEntityManager",
        transactionManagerRef = "currencyTransactionManager"
)
public class CurrencyConfiguration extends BaseConfiguration {

    @Value("${datasource.currency.url}")
    private String url;

    @Bean
    public HikariDataSource currencyDataSource() {
        return dataSource(url);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean currencyEntityManager() {
        return entityManagerFactoryBean(currencyDataSource(), "ivan.smolin.webinar.api.currency.entity");
    }

    @Bean
    public PlatformTransactionManager currencyTransactionManager() {
        return transactionManager(currencyEntityManager());
    }
}