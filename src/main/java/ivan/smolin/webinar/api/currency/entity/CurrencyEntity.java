package ivan.smolin.webinar.api.currency.entity;

import ivan.smolin.webinar.api.currency.model.CurrencyModel;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "currency")
public final class CurrencyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private boolean active;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    private List<CurrencyExchangeRateEntity> exchangeRates;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "currency_country_id")
    private CurrencyCountryEntity country;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "currency_extra_map",
            joinColumns = {@JoinColumn(name = "currency_id")},
            inverseJoinColumns = {@JoinColumn(name = "extra_id")})
    private List<CurrencyExtraEntity> extras;

    public CurrencyModel toCurrencyModel() {
        return new CurrencyModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString())
                .setCountry(this.country.toCurrencyCountryModel())
                .setExchangeRates(this.exchangeRates
                        .stream()
                        .map(CurrencyExchangeRateEntity::toCurrencyExchangeRateModel)
                        .collect(Collectors.toList())
                )
                .setExtras(this.extras
                        .stream()
                        .map(CurrencyExtraEntity::toCurrencyExtraModel)
                        .collect(Collectors.toList())
                );
    }
}