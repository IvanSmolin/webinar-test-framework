package ivan.smolin.webinar.api.currency.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.currency.entity.CurrencyEntity;
import ivan.smolin.webinar.api.currency.model.CurrencyModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.NAME_MAX_LENGTH;

@Component
public final class CurrencyRandom extends CommonRandom {

    @Autowired
    private CurrencyCountryRandom currencyCountryRandom;

    @Autowired
    private CurrencyExchangeRateRandom currencyExchangeRateRandom;

    @Autowired
    private CurrencyExtraRandom currencyExtraRandom;

    /**
     * Метод для генерации Currency Entity
     */
    public CurrencyEntity getRandomEntity() {
        return new CurrencyEntity()
                .setName(faker.name().name())
                .setActive(true)
                .setCreatedAt(getDate())
                .setCountry(currencyCountryRandom.getRandomEntity())
                .setExchangeRates(currencyExchangeRateRandom.getRandomListEntity())
                .setExtras(currencyExtraRandom.getRandomListEntity());
    }

    /**
     * Методы для генерации Currency Model
     */
    public CurrencyModel getRandomModelMax() {
        return new CurrencyModel()
                .setName(getString(NAME_MAX_LENGTH))
                .setCreatedAt(getStringDate())
                .setCountry(currencyCountryRandom.getRandomModelMax())
                .setExchangeRates(currencyExchangeRateRandom.getRandomListModel())
                .setExtras(currencyExtraRandom.getRandomListModel());
    }

    public CurrencyModel getRandomModelMin() {
        return new CurrencyModel()
                .setName(getString(CURRENCY_NAME_MIN_LENGTH))
                .setCreatedAt(getStringDate())
                .setCountry(currencyCountryRandom.getRandomModelMin())
                .setExchangeRates(currencyExchangeRateRandom.getRandomListModel())
                .setExtras(currencyExtraRandom.getRandomListModel());
    }

    public CurrencyModel getRandomModelExceeded() {
        return new CurrencyModel()
                .setName(getString(NAME_MAX_LENGTH + MIN_NUMBER))
                .setCreatedAt(getStringDate())
                .setCountry(currencyCountryRandom.getRandomModelExceeded())
                .setExchangeRates(currencyExchangeRateRandom.getRandomListModel())
                .setExtras(currencyExtraRandom.getRandomListModel());
    }

    public CurrencyModel getRandomModelLessMin() {
        return new CurrencyModel()
                .setName(getString(CURRENCY_NAME_MIN_LENGTH - MIN_NUMBER))
                .setCreatedAt(getStringDate())
                .setCountry(currencyCountryRandom.getRandomModelLessMin())
                .setExchangeRates(currencyExchangeRateRandom.getRandomListModel())
                .setExtras(currencyExtraRandom.getRandomListModel());
    }
}