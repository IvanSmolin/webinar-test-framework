package ivan.smolin.webinar.api.currency.entity;

import ivan.smolin.webinar.api.currency.model.CurrencyExtraModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "currency_extra")
public final class CurrencyExtraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public CurrencyExtraModel toCurrencyExtraModel() {
        return new CurrencyExtraModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString());
    }
}