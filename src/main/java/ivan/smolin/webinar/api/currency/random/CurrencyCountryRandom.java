package ivan.smolin.webinar.api.currency.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.currency.entity.CurrencyCountryEntity;
import ivan.smolin.webinar.api.currency.model.CurrencyCountryModel;
import org.springframework.stereotype.Component;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.NAME_MAX_LENGTH;
import static ivan.smolin.webinar.api.currency.constants.CurrencyConstants.NAME_MIN_LENGTH;

@Component
public final class CurrencyCountryRandom extends CommonRandom {

    /**
     * Метод для генерации Currency Country Entity
     */
    public CurrencyCountryEntity getRandomEntity() {
        return new CurrencyCountryEntity()
                .setName(faker.name().name())
                .setCreatedAt(getDate());
    }

    /**
     * Методы для генерации Currency Country Model
     */
    public CurrencyCountryModel getRandomModelMax() {
        return new CurrencyCountryModel()
                .setName(getString(NAME_MAX_LENGTH))
                .setCreatedAt(getStringDate());
    }

    public CurrencyCountryModel getRandomModelMin() {
        return new CurrencyCountryModel()
                .setName(getString(NAME_MIN_LENGTH))
                .setCreatedAt(getStringDate());
    }

    public CurrencyCountryModel getRandomModelExceeded() {
        return new CurrencyCountryModel()
                .setName(getString(NAME_MAX_LENGTH + MIN_NUMBER))
                .setCreatedAt(getStringDate());
    }

    public CurrencyCountryModel getRandomModelLessMin() {
        return new CurrencyCountryModel()
                .setName(getString(NAME_MIN_LENGTH - MIN_NUMBER))
                .setCreatedAt(getStringDate());
    }
}