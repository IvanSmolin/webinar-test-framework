package ivan.smolin.webinar.api.employee.model;

import lombok.Data;

@Data
public final class EmployeeResponseModel {
    private int id;
}