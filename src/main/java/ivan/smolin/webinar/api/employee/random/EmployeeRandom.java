package ivan.smolin.webinar.api.employee.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.employee.entity.EmployeeEntity;
import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.employee.constants.EmployeeConstants.NAME_MAX_LENGTH;

@Component
public final class EmployeeRandom extends CommonRandom {

    @Autowired
    private EmployeeCountryRandom employeeCountryRandom;

    @Autowired
    private EmployeeExchangeRateRandom employeeExchangeRateRandom;

    @Autowired
    private EmployeeExtraRandom employeeExtraRandom;

    /**
     * Метод для генерации Employee Entity
     */
    public EmployeeEntity getRandomEntity() {
        return new EmployeeEntity()
                .setName(faker.name().name())
                .setActive(true)
                .setCreatedAt(getDate())
                .setCountry(employeeCountryRandom.getRandomEntity())
                .setExchangeRates(employeeExchangeRateRandom.getRandomListEntity())
                .setExtras(employeeExtraRandom.getRandomListEntity());
    }

    /**
     * Методы для генерации Employee Model
     */
    public EmployeeModel getRandomModelMax() {
        return new EmployeeModel()
                .setName(getString(NAME_MAX_LENGTH))
                .setCreatedAt(getStringDate())
                .setCountry(employeeCountryRandom.getRandomModelMax())
                .setExchangeRates(employeeExchangeRateRandom.getRandomListModel())
                .setExtras(employeeExtraRandom.getRandomListModel());
    }

    public EmployeeModel getRandomModelMin() {
        return new EmployeeModel()
                .setName(getString(CURRENCY_NAME_MIN_LENGTH))
                .setCreatedAt(getStringDate())
                .setCountry(employeeCountryRandom.getRandomModelMin())
                .setExchangeRates(employeeExchangeRateRandom.getRandomListModel())
                .setExtras(employeeExtraRandom.getRandomListModel());
    }

    public EmployeeModel getRandomModelExceeded() {
        return new EmployeeModel()
                .setName(getString(NAME_MAX_LENGTH + MIN_NUMBER))
                .setCreatedAt(getStringDate())
                .setCountry(employeeCountryRandom.getRandomModelExceeded())
                .setExchangeRates(employeeExchangeRateRandom.getRandomListModel())
                .setExtras(employeeExtraRandom.getRandomListModel());
    }

    public EmployeeModel getRandomModelLessMin() {
        return new EmployeeModel()
                .setName(getString(CURRENCY_NAME_MIN_LENGTH - MIN_NUMBER))
                .setCreatedAt(getStringDate())
                .setCountry(employeeCountryRandom.getRandomModelLessMin())
                .setExchangeRates(employeeExchangeRateRandom.getRandomListModel())
                .setExtras(employeeExtraRandom.getRandomListModel());
    }
}