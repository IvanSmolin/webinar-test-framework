package ivan.smolin.webinar.api.employee.repository;

import ivan.smolin.webinar.api.employee.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends CrudRepository<EmployeeEntity, Integer> {

    @Query(value = "select * " +
            "from employee " +
            "where active = true " +
            "order by random() " +
            "limit 1",
            nativeQuery = true)
    EmployeeEntity getRandomActiveEmployee();

    @Query("SELECT e " +
            "FROM EmployeeEntity e " +
            "WHERE e.active = true")
    List<EmployeeEntity> getAllActiveEntities();

    List<EmployeeEntity> findAllByActiveTrue();

    @Query("SELECT e " +
            "FROM EmployeeEntity e " +
            "WHERE e.name = ?1")
    EmployeeEntity getByName(String id);

    EmployeeEntity findByName(String name);
}