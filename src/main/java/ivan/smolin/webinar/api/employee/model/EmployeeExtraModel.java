package ivan.smolin.webinar.api.employee.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class EmployeeExtraModel {
    private Integer id;
    private String name;

    @JsonProperty("created_at")
    private String createdAt;
}