package ivan.smolin.webinar.api.employee.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.employee.entity.EmployeeExtraEntity;
import ivan.smolin.webinar.api.employee.model.EmployeeExtraModel;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@Component
public final class EmployeeExtraRandom extends CommonRandom {

    /**
     * Метод для генерации Employee Extra Entity
     */
    private EmployeeExtraEntity getRandomEntity() {
        return new EmployeeExtraEntity()
                .setName(faker.name().name())
                .setCreatedAt(getDate());
    }

    public List<EmployeeExtraEntity> getRandomListEntity() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomEntity())
                .collect(Collectors.toList());
    }

    /**
     * Методы для генерации Employee Extra Model
     */
    private EmployeeExtraModel getRandomModel() {
        return new EmployeeExtraModel()
                .setName(faker.name().name())
                .setCreatedAt(getStringDate());
    }

    public List<EmployeeExtraModel> getRandomListModel() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomModel())
                .collect(Collectors.toList());
    }
}