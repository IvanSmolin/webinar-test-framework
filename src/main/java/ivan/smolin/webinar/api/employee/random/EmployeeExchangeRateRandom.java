package ivan.smolin.webinar.api.employee.random;

import ivan.smolin.webinar.api.common.CommonRandom;
import ivan.smolin.webinar.api.employee.entity.EmployeeExchangeRateEntity;
import ivan.smolin.webinar.api.employee.model.EmployeeExchangeRateModel;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@Component
public final class EmployeeExchangeRateRandom extends CommonRandom {

    /**
     * Метод для генерации Employee Exchange Rate Entity
     */
    private EmployeeExchangeRateEntity getRandomEntity() {
        return new EmployeeExchangeRateEntity()
                .setRate(getDouble())
                .setRateDate(LocalDate.now())
                .setCreatedAt(getDate());
    }

    public List<EmployeeExchangeRateEntity> getRandomListEntity() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomEntity())
                .collect(Collectors.toList());
    }

    /**
     * Методы для генерации Employee Exchange Rate Model
     */
    private EmployeeExchangeRateModel getRandomModel() {
        return new EmployeeExchangeRateModel()
                .setRate(getDouble())
                .setCreatedAt(getStringDate())
                .setRateDate(LocalDate.now().toString());
    }

    public List<EmployeeExchangeRateModel> getRandomListModel() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomModel())
                .collect(Collectors.toList());
    }
}