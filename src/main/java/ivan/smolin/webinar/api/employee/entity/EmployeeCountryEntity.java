package ivan.smolin.webinar.api.employee.entity;

import ivan.smolin.webinar.api.employee.model.EmployeeCountryModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "employee_country")
public final class EmployeeCountryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public EmployeeCountryModel toEmployeeCountryModel() {
        return new EmployeeCountryModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString());
    }
}