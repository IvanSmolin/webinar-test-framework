package ivan.smolin.webinar.api.employee.configuration;

import com.zaxxer.hikari.HikariDataSource;
import ivan.smolin.webinar.api.common.BaseConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;

@Configuration
@EnableJpaRepositories(
        basePackages = "ivan.smolin.webinar.api.employee.repository",
        entityManagerFactoryRef = "employeeEntityManager",
        transactionManagerRef = "employeeTransactionManager"
)
public class EmployeeConfiguration extends BaseConfiguration {

    @Value("${datasource.employee.url}")
    private String url;

    @Bean
    public HikariDataSource employeeDataSource() {
        return dataSource(url);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean employeeEntityManager() {
        return entityManagerFactoryBean(employeeDataSource(), "ivan.smolin.webinar.api.employee.entity");
    }

    @Bean
    public PlatformTransactionManager employeeTransactionManager() {
        return transactionManager(employeeEntityManager());
    }
}