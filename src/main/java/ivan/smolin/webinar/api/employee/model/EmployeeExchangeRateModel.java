package ivan.smolin.webinar.api.employee.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class EmployeeExchangeRateModel {
    private Integer id;
    private Double rate;

    @JsonProperty("rate_date")
    private String rateDate;

    @JsonProperty("created_at")
    private String createdAt;
}