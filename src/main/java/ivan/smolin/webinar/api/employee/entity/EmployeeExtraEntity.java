package ivan.smolin.webinar.api.employee.entity;

import ivan.smolin.webinar.api.employee.model.EmployeeExtraModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "employee_extra")
public final class EmployeeExtraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public EmployeeExtraModel toEmployeeExtraModel() {
        return new EmployeeExtraModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString());
    }
}