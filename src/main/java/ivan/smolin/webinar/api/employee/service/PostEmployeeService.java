package ivan.smolin.webinar.api.employee.service;

import ivan.smolin.webinar.api.common.BaseService;
import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import ivan.smolin.webinar.api.employee.model.EmployeeResponseModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class PostEmployeeService extends BaseService {

    public ResponseEntity<EmployeeResponseModel> post(EmployeeModel requestModel) {
        return postRequest(
                appProperties.getEmployeeBaseUrl(),
                requestModel,
                EmployeeResponseModel.class
        );
    }

    public ResponseEntity<String> postError(EmployeeModel requestModel) {
        return postRequest(
                appProperties.getEmployeeBaseUrl(),
                requestModel,
                String.class
        );
    }
}