package ivan.smolin.webinar.api.employee.service;

import ivan.smolin.webinar.api.common.BaseService;
import ivan.smolin.webinar.api.employee.model.EmployeeModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class GetEmployeeService extends BaseService {

    public ResponseEntity<EmployeeModel> get(long id) {
        return getRequest(
                appProperties.getEmployeeBaseUrl() + "/" + id,
                EmployeeModel.class
        );
    }

    public ResponseEntity<String> getError(long id) {
        return getError(String.valueOf(id));
    }

    public ResponseEntity<String> getError(String id) {
        return getRequest(
                appProperties.getEmployeeBaseUrl() + "/" + id,
                String.class
        );
    }
}