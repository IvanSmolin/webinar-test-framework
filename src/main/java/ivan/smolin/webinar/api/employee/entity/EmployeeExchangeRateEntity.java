package ivan.smolin.webinar.api.employee.entity;

import ivan.smolin.webinar.api.employee.model.EmployeeExchangeRateModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "employee_exchange_rate")
public final class EmployeeExchangeRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Double rate;

    @Column(name = "rate_date")
    private LocalDate rateDate;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public EmployeeExchangeRateModel toEmployeeExchangeRateModel() {
        return new EmployeeExchangeRateModel()
                .setId(this.id)
                .setRate(this.rate)
                .setRateDate(this.rateDate.toString())
                .setCreatedAt(this.createdAt.toString());
    }
}