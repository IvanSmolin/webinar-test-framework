package ivan.smolin.webinar.api.common;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_URL_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;
import static ivan.smolin.webinar.api.common.constants.RandomizerConstants.CYRILLIC;
import static ivan.smolin.webinar.api.common.constants.RandomizerConstants.DIGITS;
import static ivan.smolin.webinar.api.common.constants.RandomizerConstants.LATIN;
import static ivan.smolin.webinar.api.common.constants.RandomizerConstants.SPECIAL_CHARACTERS;

/**
 * Класс содержит общие методы для генерации случайных значений для тестирования
 */
public abstract class CommonRandom {

    @Autowired
    protected Faker faker;

    /**
     * Метод для генерации случайного вещественного числа с точностью до 5 знаков
     *
     * @return случайное значение из диапазона от 1 до 2147483647 (включительно)
     */
    protected Double getDouble() {
        return faker.number().randomDouble(MAX_LIST_LENGTH, MIN_NUMBER, MAX_URL_LENGTH);
    }

    /**
     * Метод для получения случайного индекса последовательности
     *
     * @param length принимает длину последовательности
     * @return случайный целочисленный индекс последовательности от 0 до length (исключительно)
     */
    private int getIndex(int length) {
        return faker.number().numberBetween((int) ZERO_NUMBER, length);
    }

    /**
     * Метод для генерации случайной строки определенной длины
     *
     * @param baseCharacters принимает строку, содержащую символы из которых должна состоять генерируемая строка
     * @param length         принимает длину для генерируемой строки
     * @return случайную строку определенной длины
     */
    protected String getString(String baseCharacters, int length) {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(length)
                .map(it -> String.valueOf(baseCharacters.charAt(getIndex(baseCharacters.length()))))
                .collect(Collectors.joining());
    }

    /**
     * Метод для генерации случайной строки определенной длины с любыми символами
     *
     * @param length принимает максимальную длину для генерируемой строки
     * @return случайную строку случайной длины
     */
    protected String getString(long length) {
        return getString(CYRILLIC + LATIN + SPECIAL_CHARACTERS + DIGITS, (int) length);
    }

    /**
     * Метод для генерации случайной строки определенной длины с нечисловыми символами
     *
     * @param length принимает максимальную длину для генерируемой строки
     * @return случайную строку случайной длины
     */
    public String getTextString(long length) {
        return getString(CYRILLIC + LATIN + SPECIAL_CHARACTERS, (int) length);
    }

    /**
     * Метод для генерации случайной даты
     *
     * @return случайную дату в корректном формате
     */
    protected LocalDateTime getDate() {
        return LocalDateTime.now().withNano((int) ZERO_NUMBER);
    }

    /**
     * Метод для генерации случайной даты в формате строки
     *
     * @return случайную дату в формате строки
     */
    protected String getStringDate() {
        return getDate().toString();
    }

    /**
     * Метод для генерации случайного отрицательного числа от -9 до -1
     *
     * @return случайное отрицательное число от -9 до -1
     */
    public String getNegativeNumber() {
        return "-" + faker.number().randomDigitNotZero();
    }

    /**
     * Метод для генерации случайного Email
     *
     * @param length принимает максимальную длину для генерируемой строки
     * @return случайную строку случайной длины
     */
    public String getEmail(int length) {
        return getString(LATIN, length) + "@gmail.com";
    }

    /**
     * Метод для генерации случайной числовой строки определенной длины
     *
     * @param length принимает длину для строки
     * @return случайную числовую строку определенной длины
     */
    public String getNumberString(int length) {
        return getString(DIGITS, length);
    }

    /**
     * Метод для получения случайного значения в enum
     *
     * @param enumValue принимает необходимый enum
     * @param <T>       принимает тип необходимого enum
     * @return случайное значение из переданного enum
     */
    public <T extends Enum<?>> T getEnum(Class<T> enumValue) {
        return enumValue.getEnumConstants()[getIndex(enumValue.getEnumConstants().length)];
    }
}