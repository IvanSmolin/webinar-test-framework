package ivan.smolin.webinar.api.common;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.Allure;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

public abstract class BaseService {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Autowired
    protected AppProperties appProperties;

    /**
     * Этот блок методов отвечает за отправку POST запросов
     */
    protected <T> ResponseEntity<T> postRequest(String url, Object model, Class<T> responseType) {
        return addReportDescription(
                "POST",
                url,
                model,
                testRestTemplate.postForEntity(url, model, responseType)
        );
    }

    /**
     * Этот блок методов отвечает за отправку GET запросов
     */
    protected <T> ResponseEntity<T> getRequest(String url, Class<T> responseType) {
        return addReportDescription(
                "GET",
                url,
                null,
                testRestTemplate.getForEntity(url, responseType)
        );
    }

    /**
     * Этот блок методов отвечает за отправку DELETE запросов
     */
    protected ResponseEntity<String> deleteRequest(String url) {
        return addReportDescription(
                "DELETE",
                url,
                null,
                testRestTemplate.exchange(url, HttpMethod.DELETE, HttpEntity.EMPTY, String.class)
        );
    }

    /**
     * Метод записывает описание в Allure отчет для каждого теста
     * Описание содержит используемый URL, тело запроса, если оно имелось, статус код и тело ответа, если оно имелось
     *
     * @param restType       тип REST запроса
     * @param url            URL по которому производится запрос
     * @param requestBody    тело запроса
     * @param responseEntity тело ответа со статусами и хэдерами
     * @param <T>            тип тела ответа
     * @return responseEntity
     */
    @SneakyThrows
    private <T> ResponseEntity<T> addReportDescription(String restType, String url, Object requestBody, ResponseEntity<T> responseEntity) {
        ObjectMapper mapper = new ObjectMapper();
        String description = "<b>Используемый URL:</b> " + restType + "  " + url;

        description += requestBody != null
                ? "<br><br><b>Request Body:</b> " + mapper.writeValueAsString(requestBody)
                : "";

        description += "<br><br><b>Response Body:</b>" +
                "<br><i><b>Status Code:</b></i> " + responseEntity.getStatusCode();

        description += responseEntity.hasBody()
                ? "<br><i><b>Body:</b></i> " + mapper.writeValueAsString(responseEntity.getBody())
                : "";

        Allure.description(description);

        return responseEntity;
    }
}