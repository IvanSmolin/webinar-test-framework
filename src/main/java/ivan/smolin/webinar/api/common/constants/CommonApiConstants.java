package ivan.smolin.webinar.api.common.constants;

public final class CommonApiConstants {
    public static final long INTEGER_MAX_LENGTH = 2_147_483_647L;
    public static final long INTEGER_EXCEEDED_LENGTH = 2_147_483_648L;
    public static final long ZERO_NUMBER = 0L;
    public static final long MIN_NUMBER = 1L;
    public static final long MAX_URL_LENGTH = 1024L;
    public static final int MAX_LIST_LENGTH = 10;

    public static String fieldIsRequired(String field) {
        return "Поле \"" + field + "\" обязательное";
    }

    public static String objectIsRequired(String object) {
        return "Объект \"" + object + "\" должен быть заполнен";
    }

    public static String listOfObjectsIsRequired(String list) {
        return "Список объектов \"" + list + "\" должен быть заполнен";
    }

    public static String maxLength(int length) {
        return "Максимальная длина " + length + " символов";
    }

    public static String minLength(int length) {
        return "Минимальная длина " + length + " символов";
    }
}