package ivan.smolin.webinar.api.common;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public final class AppProperties {

    @Value("${baseUrl.currency}")
    private String currencyBaseUrl;

    @Value("${baseUrl.employee}")
    private String employeeBaseUrl;

    @Value("${baseUrl.account}")
    private String accountBaseUrl;
}