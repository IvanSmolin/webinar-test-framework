package ivan.smolin.webinar.api.common;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;

/**
 * Class contains methods for HTTP ResponseEntity assertions for status codes 404, 400, 200
 */
public final class CustomHttpResponseAssertion {

    /**
     * 404 - Not Found
     */
    public static <T> void assertNotFoundResponse(ResponseEntity<T> responseEntity) {
        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND),
                () -> assertThat(responseEntity.getBody()).isNotNull()
        );
    }

    public static void assertNotFoundStringResponse(ResponseEntity<String> responseEntity) {
        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND),
                () -> assertThat(responseEntity.getBody()).isNotEmpty()
        );
    }

    /**
     * 400 - Bad Request
     */
    public static <T> void assertBadRequestResponse(ResponseEntity<T> responseEntity) {
        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST),
                () -> assertThat(responseEntity.getBody()).isNotNull()
        );
    }

    public static void assertBadRequestStringResponse(ResponseEntity<String> responseEntity) {
        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST),
                () -> assertThat(responseEntity.getBody()).isNotEmpty()
        );
    }

    @SneakyThrows
    public static void assertResponseWithErrorMessages(ResponseEntity<String> responseEntity, Map<String, String> errorMap) {
        Map<String, String> map = new ObjectMapper().readValue(responseEntity.getBody(), new TypeReference<Map<String, String>>() {});

        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST),
                () -> assertThat(map).isEqualTo(errorMap)
        );
    }

    /**
     * 200 - OK
     */
    public static <T> void assertOkResponse(ResponseEntity<T> responseEntity, Object model) {
        assertAll(
                () -> assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK),
                () -> assertThat(responseEntity.getBody()).isEqualTo(model)
        );
    }

    /**
     * 201 - Created
     */
    public static <T> void assertCreatedResponse(ResponseEntity<T> responseEntity) {
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
    }
}