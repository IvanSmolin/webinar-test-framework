package ivan.smolin.webinar.api.common;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Базовый класс конфигурации для подключения к необходимой БД
 */
public abstract class BaseConfiguration {

    @Value("${datasource.username}")
    private String username;

    @Value("${datasource.password}")
    private String password;

    @Value("${datasource.maximumPoolSize}")
    private int maximumPoolSize;

    @Value("${datasource.connectionTimeout}")
    private int connectionTimeout;

    /**
     * Метод определяет HikariDataSource, в котором устанавливается URL необходимой БД, логин и пароль для БД,
     * максимальное количество подключений к БД и время которое мы пытаемся подключиться к БД
     *
     * @param url принимает JDBC URL для необходимой БД (хранятся в ресурсах application.yml)
     * @return возвращает сконфигурированный HikariDataSource
     */
    protected HikariDataSource dataSource(String url) {
        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setJdbcUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setMaximumPoolSize(maximumPoolSize);
        dataSource.setConnectionTimeout(connectionTimeout);

        return dataSource;
    }

    /**
     * Метод определяет LocalContainerEntityManagerFactoryBean, который необходим для работы с Entity и их поиска
     * для подключаемой БД
     *
     * @param dataSource    принимает сконфигурированный HikariDataSource
     * @param packageToScan принимает название пакета где хранятся Entity, необходимые для взаимодействия с подключаемой БД
     * @return сконфигурированный LocalContainerEntityManagerFactoryBean
     */
    protected LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(HikariDataSource dataSource, String packageToScan) {
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setPackagesToScan(packageToScan);

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        factoryBean.setJpaVendorAdapter(vendorAdapter);

        return factoryBean;
    }

    /**
     * Метод определяет PlatformTransactionManager, необходимый для работы с транзакциями БД
     *
     * @param entityManagerFactoryBean принимает сконфигурированный LocalContainerEntityManagerFactoryBean
     * @return сконфигурированный PlatformTransactionManager
     */
    protected PlatformTransactionManager transactionManager(LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactoryBean.getObject());

        return transactionManager;
    }
}