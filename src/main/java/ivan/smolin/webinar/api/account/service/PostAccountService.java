package ivan.smolin.webinar.api.account.service;

import ivan.smolin.webinar.api.account.model.AccountModel;
import ivan.smolin.webinar.api.account.model.AccountResponseModel;
import ivan.smolin.webinar.api.common.BaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class PostAccountService extends BaseService {

    public ResponseEntity<AccountResponseModel> post(AccountModel requestModel) {
        return postRequest(
                appProperties.getAccountBaseUrl(),
                requestModel,
                AccountResponseModel.class
        );
    }

    public ResponseEntity<String> postError(AccountModel requestModel) {
        return postRequest(
                appProperties.getAccountBaseUrl(),
                requestModel,
                String.class
        );
    }
}