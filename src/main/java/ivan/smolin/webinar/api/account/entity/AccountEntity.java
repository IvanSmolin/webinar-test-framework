package ivan.smolin.webinar.api.account.entity;

import ivan.smolin.webinar.api.account.model.AccountModel;
import lombok.Data;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Entity
@Table(name = "account")
public final class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column
    private boolean active;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private List<AccountExchangeRateEntity> exchangeRates;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_country_id")
    private AccountCountryEntity country;

    @ManyToMany(cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "account_extra_map",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "extra_id")})
    private List<AccountExtraEntity> extras;

    public AccountModel toAccountModel() {
        return new AccountModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString())
                .setCountry(this.country.toAccountCountryModel())
                .setExchangeRates(this.exchangeRates
                        .stream()
                        .map(AccountExchangeRateEntity::toAccountExchangeRateModel)
                        .collect(Collectors.toList())
                )
                .setExtras(this.extras
                        .stream()
                        .map(AccountExtraEntity::toAccountExtraModel)
                        .collect(Collectors.toList())
                );
    }
}