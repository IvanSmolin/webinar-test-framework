package ivan.smolin.webinar.api.account.entity;

import ivan.smolin.webinar.api.account.model.AccountCountryModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "account_country")
public final class AccountCountryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public AccountCountryModel toAccountCountryModel() {
        return new AccountCountryModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString());
    }
}