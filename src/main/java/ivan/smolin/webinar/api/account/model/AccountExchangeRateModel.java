package ivan.smolin.webinar.api.account.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class AccountExchangeRateModel {
    private Integer id;
    private Double rate;

    @JsonProperty("rate_date")
    private String rateDate;

    @JsonProperty("created_at")
    private String createdAt;
}