package ivan.smolin.webinar.api.account.random;

import ivan.smolin.webinar.api.account.entity.AccountExchangeRateEntity;
import ivan.smolin.webinar.api.account.model.AccountExchangeRateModel;
import ivan.smolin.webinar.api.common.CommonRandom;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MAX_LIST_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.ZERO_NUMBER;

@Component
public final class AccountExchangeRateRandom extends CommonRandom {

    /**
     * Метод для генерации Account Exchange Rate Entity
     */
    private AccountExchangeRateEntity getRandomEntity() {
        return new AccountExchangeRateEntity()
                .setRate(getDouble())
                .setRateDate(LocalDate.now())
                .setCreatedAt(getDate());
    }

    public List<AccountExchangeRateEntity> getRandomListEntity() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomEntity())
                .collect(Collectors.toList());
    }

    /**
     * Методы для генерации Account Exchange Rate Model
     */
    private AccountExchangeRateModel getRandomModel() {
        return new AccountExchangeRateModel()
                .setRate(getDouble())
                .setCreatedAt(getStringDate())
                .setRateDate(LocalDate.now().toString());
    }

    public List<AccountExchangeRateModel> getRandomListModel() {
        return Stream.iterate(ZERO_NUMBER, n -> n + MIN_NUMBER)
                .limit(MAX_LIST_LENGTH)
                .map(it -> getRandomModel())
                .collect(Collectors.toList());
    }
}