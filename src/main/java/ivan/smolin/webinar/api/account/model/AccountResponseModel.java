package ivan.smolin.webinar.api.account.model;

import lombok.Data;

@Data
public final class AccountResponseModel {
    private int id;
}