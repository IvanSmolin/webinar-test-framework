package ivan.smolin.webinar.api.account.entity;

import ivan.smolin.webinar.api.account.model.AccountExchangeRateModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "account_exchange_rate")
public final class AccountExchangeRateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private Double rate;

    @Column(name = "rate_date")
    private LocalDate rateDate;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public AccountExchangeRateModel toAccountExchangeRateModel() {
        return new AccountExchangeRateModel()
                .setId(this.id)
                .setRate(this.rate)
                .setRateDate(this.rateDate.toString())
                .setCreatedAt(this.createdAt.toString());
    }
}