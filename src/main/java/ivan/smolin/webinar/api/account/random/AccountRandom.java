package ivan.smolin.webinar.api.account.random;

import ivan.smolin.webinar.api.account.entity.AccountEntity;
import ivan.smolin.webinar.api.account.model.AccountModel;
import ivan.smolin.webinar.api.common.CommonRandom;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static ivan.smolin.webinar.api.account.constants.AccountConstants.CURRENCY_NAME_MIN_LENGTH;
import static ivan.smolin.webinar.api.account.constants.AccountConstants.NAME_MAX_LENGTH;
import static ivan.smolin.webinar.api.common.constants.CommonApiConstants.MIN_NUMBER;

@Component
public final class AccountRandom extends CommonRandom {

    @Autowired
    private AccountCountryRandom accountCountryRandom;

    @Autowired
    private AccountExchangeRateRandom accountExchangeRateRandom;

    @Autowired
    private AccountExtraRandom accountExtraRandom;

    /**
     * Метод для генерации Account Entity
     */
    public AccountEntity getRandomEntity() {
        return new AccountEntity()
                .setName(faker.name().name())
                .setActive(true)
                .setCreatedAt(getDate())
                .setCountry(accountCountryRandom.getRandomEntity())
                .setExchangeRates(accountExchangeRateRandom.getRandomListEntity())
                .setExtras(accountExtraRandom.getRandomListEntity());
    }

    /**
     * Методы для генерации Account Model
     */
    public AccountModel getRandomModelMax() {
        return new AccountModel()
                .setName(getString(NAME_MAX_LENGTH))
                .setCreatedAt(getStringDate())
                .setCountry(accountCountryRandom.getRandomModelMax())
                .setExchangeRates(accountExchangeRateRandom.getRandomListModel())
                .setExtras(accountExtraRandom.getRandomListModel());
    }

    public AccountModel getRandomModelMin() {
        return new AccountModel()
                .setName(getString(CURRENCY_NAME_MIN_LENGTH))
                .setCreatedAt(getStringDate())
                .setCountry(accountCountryRandom.getRandomModelMin())
                .setExchangeRates(accountExchangeRateRandom.getRandomListModel())
                .setExtras(accountExtraRandom.getRandomListModel());
    }

    public AccountModel getRandomModelExceeded() {
        return new AccountModel()
                .setName(getString(NAME_MAX_LENGTH + MIN_NUMBER))
                .setCreatedAt(getStringDate())
                .setCountry(accountCountryRandom.getRandomModelExceeded())
                .setExchangeRates(accountExchangeRateRandom.getRandomListModel())
                .setExtras(accountExtraRandom.getRandomListModel());
    }

    public AccountModel getRandomModelLessMin() {
        return new AccountModel()
                .setName(getString(CURRENCY_NAME_MIN_LENGTH - MIN_NUMBER))
                .setCreatedAt(getStringDate())
                .setCountry(accountCountryRandom.getRandomModelLessMin())
                .setExchangeRates(accountExchangeRateRandom.getRandomListModel())
                .setExtras(accountExtraRandom.getRandomListModel());
    }
}