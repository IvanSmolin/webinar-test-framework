package ivan.smolin.webinar.api.account.entity;

import ivan.smolin.webinar.api.account.model.AccountExtraModel;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "account_extra")
public final class AccountExtraEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column
    private String name;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    public AccountExtraModel toAccountExtraModel() {
        return new AccountExtraModel()
                .setId(this.id)
                .setName(this.name)
                .setCreatedAt(this.createdAt.toString());
    }
}