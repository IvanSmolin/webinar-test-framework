package ivan.smolin.webinar.api.account.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public final class AccountModel {
    private Integer id;
    private String name;
    private final boolean active = true;
    private List<AccountExchangeRateModel> exchangeRates;
    private AccountCountryModel country;
    private List<AccountExtraModel> extras;

    @JsonProperty("created_at")
    private String createdAt;
}