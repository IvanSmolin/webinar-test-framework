package ivan.smolin.webinar.api.account.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public final class AccountCountryModel {
    private Integer id;
    private String name;

    @JsonProperty("created_at")
    private String createdAt;
}