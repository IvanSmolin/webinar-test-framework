package ivan.smolin.webinar.api.account.service;

import ivan.smolin.webinar.api.account.model.AccountModel;
import ivan.smolin.webinar.api.common.BaseService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public final class GetAccountService extends BaseService {

    public ResponseEntity<AccountModel> get(long id) {
        return getRequest(
                appProperties.getAccountBaseUrl() + "/" + id,
                AccountModel.class
        );
    }

    public ResponseEntity<String> getError(long id) {
        return getError(String.valueOf(id));
    }

    public ResponseEntity<String> getError(String id) {
        return getRequest(
                appProperties.getAccountBaseUrl() + "/" + id,
                String.class
        );
    }
}